package common;

public class TelecomDTO {
	
	public TelecomDTO() {}
	
	private int companyNo;
	private String companyNm;
	public TelecomDTO(int companyNo, String companyNm, String companyAdd) {
		super();
		this.companyNo = companyNo;
		this.companyNm = companyNm;
		this.companyAdd = companyAdd;
	}
	public int getCompanyNo() {
		return companyNo;
	}
	public void setCompanyNo(int companyNo) {
		this.companyNo = companyNo;
	}
	public String getCompanyNm() {
		return companyNm;
	}
	public void setCompanyNm(String companyNm) {
		this.companyNm = companyNm;
	}
	public String getCompanyAdd() {
		return companyAdd;
	}
	public void setCompanyAdd(String companyAdd) {
		this.companyAdd = companyAdd;
	}
	private String companyAdd;
	@Override
	public String toString() {
		return "telecomDTO [companyNo=" + companyNo + ", companyNm=" + companyNm + ", companyAdd=" + companyAdd + "]";
	}

}
