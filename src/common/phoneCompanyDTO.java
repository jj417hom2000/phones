package common;

public class phoneCompanyDTO {
	
	public phoneCompanyDTO() {}
	
	private int companyNo;
	private String companyName;
	private String companyAdd;
	public int getCompanyNo() {
		return companyNo;
	}
	public void setCompanyNo(int companyNo) {
		this.companyNo = companyNo;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyAdd() {
		return companyAdd;
	}
	public void setCompanyAdd(String companyAdd) {
		this.companyAdd = companyAdd;
	}
	@Override
	public String toString() {
		return "phoneCompanyDTO [companyNo=" + companyNo + ", companyName=" + companyName + ", companyAdd=" + companyAdd
				+ "]";
	}
	public phoneCompanyDTO(int companyNo, String companyName, String companyAdd) {
		super();
		this.companyNo = companyNo;
		this.companyName = companyName;
		this.companyAdd = companyAdd;
	}
	
	
	
	

}
