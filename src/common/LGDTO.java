package common;

public class LGDTO {
	
	public LGDTO () {}
	
	private int feeNo;
	private String feeNm;
	private String price;
	private String gift;
	private String communicationType;
	private String feeData;
	
	
	
	public String getCommunicationType() {
		return communicationType;
	}
	public LGDTO(int feeNo, String feeNm, String price, String gift, String communicationType, String feeData) {
		super();
		this.feeNo = feeNo;
		this.feeNm = feeNm;
		this.price = price;
		this.gift = gift;
		this.communicationType = communicationType;
		this.feeData = feeData;
	}
	public void setCommunicationType(String communicationType) {
		this.communicationType = communicationType;
	}
	public String getFeeData() {
		return feeData;
	}
	public void setFeeData(String feeData) {
		this.feeData = feeData;
	}
	public int getFeeNo() {
		return feeNo;
	}
	public void setFeeNo(int feeNo) {
		this.feeNo = feeNo;
	}
	public String getFeeNm() {
		return feeNm;
	}
	public void setFeeNm(String feeNm) {
		this.feeNm = feeNm;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getGift() {
		return gift;
	}
	public void setGift(String gift) {
		this.gift = gift;
	}
	public String getCommuniType() {
		return communicationType;
	}
	public void setCommuniType(String communiType) {
		this.communicationType = communiType;
	}
	@Override
	public String toString() {
		return "LGDTO [feeNo=" + feeNo + ", feeNm=" + feeNm + ", price=" + price + ", gift=" + gift
				+ ", communicationType=" + communicationType + ", feeData=" + feeData + "]";
	}
	
	
	
	
	
	
	
	

}
