package common;

public class phoneDTO {
	public phoneDTO() {}
	
	private int phoneNo;
	private String operationSystem;
	private String FactoryPrice;
	private String phonePriceWithFee;
	private int companyCode;
	private String InternalStorage;
	private String communicationType;
	private String phoneName;
	
	
	public phoneDTO(int phoneNo, String operationSystem, String factoryPrice, String phonePriceWithFee, int companyCode,
			String internalStorage, String communicationType, String phoneName) {
		super();
		this.phoneNo = phoneNo;
		this.operationSystem = operationSystem;
		FactoryPrice = factoryPrice;
		this.phonePriceWithFee = phonePriceWithFee;
		this.companyCode = companyCode;
		this.InternalStorage = internalStorage;
		this.communicationType = communicationType;
		this.phoneName = phoneName;
	}
	public String getPhoneName() {
		return phoneName;
	}
	public void setPhoneName(String phoneName) {
		this.phoneName = phoneName;
	}
	
	public String getCommunicationType() {
		return communicationType;
	}
	public void setCommunicationType(String communicationType) {
		this.communicationType = communicationType;
	}
	public int getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(int phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getOperationSystem() {
		return operationSystem;
	}
	public void setOperationSystem(String operationSystem) {
		this.operationSystem = operationSystem;
	}
	public String getFactoryPrice() {
		return FactoryPrice;
	}
	public void setFactoryPrice(String factoryPrice) {
		FactoryPrice = factoryPrice;
	}
	public String getPhonePriceWithFee() {
		return phonePriceWithFee;
	}
	public void setPhonePriceWithFee(String phonePriceWithFee) {
		this.phonePriceWithFee = phonePriceWithFee;
	}
	public int getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(int companyCode) {
		this.companyCode = companyCode;
	}
	public String getInternalStorage() {
		return InternalStorage;
	}
	public void setInternalStorage(String internalStorage) {
		InternalStorage = internalStorage;
	}
	@Override
	public String toString() {
		return "phoneDTO [phoneNo=" + phoneNo + ", operationSystem=" + operationSystem + ", FactoryPrice="
				+ FactoryPrice + ", phonePriceWithFee=" + phonePriceWithFee + ", companyCode=" + companyCode
				+ ", InternalStorage=" + InternalStorage + ", communicationType=" + communicationType + ", phoneName="
				+ phoneName + "]";
	}
	

	

}
