package common;

public class KTDTO {
	
	public KTDTO() {}
	
	private int feeNo;
	private String feeNm;
	private String price;
	private String gift;
	private String communicationType;
	private String feedata;
	
	
	
	public int getFeeNo() {
		return feeNo;
	}
	public KTDTO(int feeNo, String feeNm, String price, String gift, String communicationType, String feedata) {
		super();
		this.feeNo = feeNo;
		this.feeNm = feeNm;
		this.price = price;
		this.gift = gift;
		this.communicationType = communicationType;
		this.feedata = feedata;
	}
	public void setFeeNo(int feeNo) {
		this.feeNo = feeNo;
	}
	public String getFeeNm() {
		return feeNm;
	}
	public void setFeeNm(String feeNm) {
		this.feeNm = feeNm;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getGift() {
		return gift;
	}
	public void setGift(String gift) {
		this.gift = gift;
	}
	public String getCommuniType() {
		return communicationType;
	}
	public void setCommuniType(String communiType) {
		this.communicationType = communiType;
	}
	@Override
	public String toString() {
		return "KTDTO [feeNo=" + feeNo + ", feeNm=" + feeNm + ", price=" + price + ", gift=" + gift
				+ ", communicationType=" + communicationType + ", feedata=" + feedata + "]";
	}
	public KTDTO(int feeNo, String feeNm, String price, String gift, String communiType) {
		super();
		this.feeNo = feeNo;
		this.feeNm = feeNm;
		this.price = price;
		this.gift = gift;
		this.communicationType = communiType;
	}
	public String getFeedata() {
		return feedata;
	}
	public void setFeedata(String feedata) {
		this.feedata = feedata;
	}
	
	
	
	
	
	
	

}
