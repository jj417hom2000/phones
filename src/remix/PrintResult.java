package remix;

import java.util.List;

import common.KTDTO;
import common.LGDTO;
import common.SKTDTO;
import common.TelecomDTO;
import common.phoneCompanyDTO;
import common.phoneDTO;

public class PrintResult {

	public void printSkList(List<SKTDTO> skList) {
		for(SKTDTO menu : skList) {
			System.out.println(menu);
		}
	}
	
	
	public void printKtList(List<KTDTO> ktList) {
		for(KTDTO menu : ktList) {
			System.out.println(menu);
		}
	}
	
	public void printLgList(List<LGDTO> lgList) {
		for(LGDTO menu : lgList) {
			System.out.println(menu);
		}
	}
	
	public void printPhoneComapnyList(List<phoneCompanyDTO> phoneCompanyList) {
		for(phoneCompanyDTO menu : phoneCompanyList) {
			System.out.println(menu);
		}
	}
	
	public void printTelecomList(List<TelecomDTO> telecomList) {
		for(TelecomDTO menu : telecomList) {
			System.out.println(menu);
		}
	}
	
	public void printPhoneList(List<phoneDTO> phoneList) {
		for(phoneDTO menu : phoneList) {
			System.out.println(menu);
		}
	}
	

/*	public void printErrorMessage(String errorCode) {
		String errorMessage = "";
		switch(errorCode) {
		case "selectList" : errorMessage = "메뉴 목록 조회에 실패하였습니다."; break;
		case "selectOne" : errorMessage = "메뉴 조회에 실패하였습니다."; break;
		case "insert" : errorMessage = "메뉴 등록에 실패하였습니다."; break;
		case "update" : errorMessage = "메뉴 수정에 실패하였습니다."; break;
		case "delete" : errorMessage = "메뉴 삭제에 실패하였습니다."; break;
		}
		
		System.out.println(errorMessage);
	}

	public void printSuccessMessage(String successCode) {
		String successMessage = "";
		switch(successCode) {
		case "insert" : successMessage = "메뉴 등록에 성공하였습니다."; break;
		case "update" : successMessage = "메뉴 수정에 성공하였습니다."; break;
		case "delete" : successMessage = "메뉴 삭제에 성공하였습니다."; break;
		}
		
		System.out.println(successMessage);*/
	}


}




