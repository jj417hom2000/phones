package remix;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Application {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		Controller controller = new Controller();
		
		do {
			System.out.println("핸드폰을 사기위한 당신의 선택");
			System.out.println("1. 통신사조회");
			System.out.println("2. 제조사조회");
			System.out.println("3. 핸드폰조회");
			System.out.println("4. SKT조회");
			System.out.println("5. KT조회");
			System.out.println("6. LG조회");
			System.out.println("7. 추가메뉴");
			System.out.println("8. 수정메뉴");
			System.out.println("9. 삭제메뉴");
			System.out.println("취소");
			int no = sc.nextInt();
			
			switch(no) {
			
			case 1 : controller.selectComSubMenu(); break;
			case 2 : controller.selectFactorySubMenu(); break;
			case 3 : controller.selectPhoneSubMenu(); break;
			case 4 : controller.selectSKTSubMenu(); break;
			case 5 : controller.selectKTSubMenu(); break;
			case 6 : controller.selectLGSubMenu(); break;
			case 7 : controller.input();break;
			case 8 : controller.update(); break;
			case 9 : controller.delete(); break;
			case 10 : System.out.println("안사요!");
			return;
			
			
			}
			
		} while(true);

}
	
	
	private static Map<String, String> inputCode()	{
		
		Scanner sc = new Scanner(System.in);
		System.out.println("코드를 입력하시오.");
		String code = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("code", code);
		
		return parameter;
		
	}
	
	
	private static Map<String, String> inputTelecoms() {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("이름을 입력하시오.");
		String name = sc.nextLine();
		System.out.println("통신 방식을 입력하시오.");
		String communicationType = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("companyNm", name);
		parameter.put("companyAdd", communicationType);
		
		return parameter;
	}
	
	private static Map<String, String> inputFees() {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("요금제 이름을 입력하시오.");
		String name = sc.nextLine();
		System.out.println("요금제 요금을 입력하시오.");
		String price = sc.nextLine();
		System.out.println("혜택을 입력하시오.");
		String gift = sc.nextLine();
		System.out.println("통신방식을 입력하시오.");
		String commnunicationType = sc.nextLine();
		System.out.println("데이터양을 입력하시오.");
		String feedata = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("feeNm", name);
		parameter.put("price", price);
		parameter.put("gift", gift);
		parameter.put("commnunicationType", commnunicationType);
		parameter.put("feedata", feedata);
		return parameter;
		
	}
	
	private static Map<String, String> inputCompanys() {

		Scanner sc = new Scanner(System.in);
		System.out.println("이름을 입력하시오.");
		String name = sc.nextLine();
		System.out.println("주소를 입력하시오.");
		String companyAdd = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("companyNm", name);
		parameter.put("companyAdd", companyAdd);
		
		return parameter;
		
	}
	
	
	private static Map<String, String> inputPhones() {

		Scanner sc = new Scanner(System.in);
		
		
		System.out.println("요금제 이름을 입력하시오.");
		String name = sc.nextLine();
		System.out.println("요금제 요금을 입력하시오.");
		String price = sc.nextLine();
		System.out.println("혜택을 입력하시오.");
		String gift = sc.nextLine();
		System.out.println("통신방식을 입력하시오.");
		String commnunicationType = sc.nextLine();
		System.out.println("데이터양을 입력하시오.");
		String feedata = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
	
		parameter.put("feeNm", name);
		parameter.put("price", price);
		parameter.put("gift", gift);
		parameter.put("commnunicationType", commnunicationType);
		parameter.put("feedata", feedata);
		return parameter;
		
	}
	
	private static Map<String, String> ModifyCompanys() {
		Scanner sc = new Scanner(System.in);
		System.out.println("코드 입력");
		String code = sc.nextLine();
		System.out.println("이름 입력");
		String name = sc.nextLine();
		System.out.println("통신 방식 입력");
		String communicationType = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("code", code);
		parameter.put("name", name);
		parameter.put("communicationType", communicationType);
		return parameter;
	}
	
	
	private static Map<String, String> ModifyFees() {
		Scanner sc = new Scanner(System.in);
		System.out.println("코드를 입력하시오");
		String code = sc.nextLine();
		System.out.println("요금제 이름을 입력하시오.");
		String name = sc.nextLine();
		System.out.println("요금제 요금을 입력하시오.");
		String price = sc.nextLine();
		System.out.println("혜택을 입력하시오.");
		String gift = sc.nextLine();
		System.out.println("통신방식을 입력하시오.");
		String commnunicationType = sc.nextLine();
		System.out.println("데이터양을 입력하시오.");
		String feedata = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("code", code);
		parameter.put("feeNm", name);
		parameter.put("price", price);
		parameter.put("gift", gift);
		parameter.put("commnunicationType", commnunicationType);
		parameter.put("feedata", feedata);
		return parameter;
	}
	
	private static Map<String, String> Modifyphones() {
		Scanner sc = new Scanner(System.in);
		System.out.println("코드를 입력하시오");
		String code = sc.nextLine();
		System.out.println("요금제 이름을 입력하시오.");
		String name = sc.nextLine();
		System.out.println("요금제 요금을 입력하시오.");
		String price = sc.nextLine();
		System.out.println("혜택을 입력하시오.");
		String gift = sc.nextLine();
		System.out.println("통신방식을 입력하시오.");
		String commnunicationType = sc.nextLine();
		System.out.println("데이터양을 입력하시오.");
		String feedata = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("code", code);
		parameter.put("feeNm", name);
		parameter.put("price", price);
		parameter.put("gift", gift);
		parameter.put("commnunicationType", commnunicationType);
		parameter.put("feedata", feedata);
		return parameter;
	}
	
	
}
