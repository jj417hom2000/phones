package remix;

import static common.Template.getSqlSession;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import common.KTDTO;
import common.LGDTO;
import common.Mappers;
import common.SKTDTO;
import common.TelecomDTO;
import common.phoneCompanyDTO;
import common.phoneDTO;

public class Services {
	
	private Mappers mappers;
	
	public List<TelecomDTO> selectTelecom() {
		SqlSession sqlSession = getSqlSession();
		
		mappers = sqlSession.getMapper(Mappers.class);
		List<TelecomDTO> telecomList = mappers.selectTelecomMenu();
		
		sqlSession.close();
		
		return telecomList;
	}

	public List<phoneDTO> selectPhoneMenu() {
		SqlSession sqlSession = getSqlSession();
		
		mappers = sqlSession.getMapper(Mappers.class);
		List<phoneDTO> phoneList = mappers.selectPhoneMenu();
		
		sqlSession.close();
		
		return phoneList;
	}
	
	public List<phoneCompanyDTO> selectPhoneCompanyMenu() {
		SqlSession sqlSession = getSqlSession();
		
		mappers = sqlSession.getMapper(Mappers.class);
		List<phoneCompanyDTO> phoneCompanyList = mappers.selectPhoneCompanyMenu();
		
		sqlSession.close();
		
		return phoneCompanyList;
		}
	
	public List<KTDTO> selectKT() {
		SqlSession sqlSession = getSqlSession();
		
		mappers = sqlSession.getMapper(Mappers.class);
		List<KTDTO> KTList = mappers.selectKTMenu();
		
		sqlSession.close();
		
		return KTList;
		}
	
	public List<SKTDTO> selectSKT() {
		SqlSession sqlSession = getSqlSession();
		
		mappers = sqlSession.getMapper(Mappers.class);
		List<SKTDTO> SKTList = mappers.selectSKTMenu();
		
		sqlSession.close();
		
		return SKTList;
		}
	public List<LGDTO> selectLG() {
		SqlSession sqlSession = getSqlSession();
		
		mappers = sqlSession.getMapper(Mappers.class);
		List<LGDTO> LGList = mappers.selectLGMenu();
		
		sqlSession.close();
		
		return LGList;
		}
	
	public 
	
	
	
	
	
	
	
}
	
