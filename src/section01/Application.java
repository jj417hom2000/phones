package section01;

import java.util.Scanner;

import common.KTDTO;
import common.LGDTO;
import common.SKTDTO;
import common.TelecomDTO;
import common.phoneCompanyDTO;
import common.phoneDTO;

public class Application {
	
	public static void main(String[] args) {
		
		
		Scanner sc = new Scanner(System.in);
		
		do {
			System.out.println("핸드폰을 사기위한 당신의 선택");
			System.out.println("1. 통신사조회");
			System.out.println("2. 제조사조회");
			System.out.println("3. 핸드폰조회");
			System.out.println("4. SKT조회");
			System.out.println("5. KT조회");
			System.out.println("6. LG조회");
			int no = sc.nextInt();
			
			switch(no) {
			
			case 1 : selectComSubMenu(); break;
			case 2 : selectFactorySubMenu(); break;
			case 3 : selectPhoneSubMenu(); break;
			case 4 : selectSKTSubMenu(); break;
			case 5 : selectKTSubMenu(); break;
			case 6 : selectLGSbuMenu(); break;
			case 9 : System.out.println("안사요!");
			return;
			
			
			}
			
		} while(true);
		
		
		
		
		
	}

	private static void selectTelecomSubMenu() {
		
		Scanner sc = new Scanner(System.in);
		SqlBuilderService sqlBuilderService = new SqlBuilderService();
		
		do {
			System.out.println("회사선택을 해보자=========");
			System.out.println("1. 추가");
			System.out.println("2. 수정");
			System.out.println("3. 조회");
			
			int no = sc.nextInt();
			
			switch(no) {
			case 1 : SqlBuilderService.registcom(inputcompany()); break;
			
			
			
			
			}
			
			
			
			
			
			
			
		} while(true);
		
		
		
		
	}

	private static void selectComSubMenu() {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	
	
	
	
	
	
//-------------------------------------------------insert	--------------------------

	
	
private static TelecomDTO inputcompany() {
	
	Scanner sc = new Scanner(System.in);

	System.out.print("등록할 통신회사의 이름을 입력하시오. ");
	String name = sc.nextLine();
	System.out.print("등록할 통신회사의 주소를 입력하시오. ");
	String add = sc.nextLine();
	
	TelecomDTO telecomDTO = new TelecomDTO();
	telecomDTO.setCompanyNm(name);
	telecomDTO.setCompanyAdd(add);
	
	return telecomDTO;

}
	
	
	
private static SKTDTO inputfee() {
		
	Scanner sc = new Scanner(System.in);
	SqlBuilderService sqlBuilderService = new SqlBuilderService();
	System.out.print("요금제 이름을 입력하시오 ");
	String name = sc.nextLine();
	System.out.print("월 요금을 입력하시오 ");
	String price = sc.nextLine();
	System.out.print("혜택을 입력하시오");
	String gift = sc.nextLine();
	System.out.print("통신방식을 입력하시오");
	String commutype = sc.nextLine();
	System.out.println("데이터의 양을 입력하시오");
	String data = sc.nextLine();
	
	SKTDTO skt = new SKTDTO();
	skt.setFeeNm(name);
	skt.setPrice(price);
	skt.setGift(gift);
	skt.setCommuniType(commutype);
	skt.setFeedata(data);
	return skt;
	}
	
private static KTDTO inputfee2() {
	
	Scanner sc = new Scanner(System.in);
	SqlBuilderService sqlBuilderService = new SqlBuilderService();
	System.out.print("요금제 이름을 입력하시오 ");
	String name = sc.nextLine();
	System.out.print("월 요금을 입력하시오 ");
	String price = sc.nextLine();
	System.out.print("혜택을 입력하시오");
	String gift = sc.nextLine();
	System.out.print("통신방식을 입력하시오");
	String commutype = sc.nextLine();
	System.out.println("데이터의 양을 입력하시오");
	String data = sc.nextLine();
	
	KTDTO kt = new KTDTO();
	kt.setFeeNm(name);
	kt.setPrice(price);
	kt.setGift(gift);
	kt.setCommuniType(commutype);
	kt.setFeedata(data);
	return kt;
	}
private static LGDTO inputfee3() {
	

	Scanner sc = new Scanner(System.in);
	SqlBuilderService sqlBuilderService = new SqlBuilderService();
	System.out.print("요금제 이름을 입력하시오 ");
	String name = sc.nextLine();
	System.out.print("월 요금을 입력하시오 ");
	String price = sc.nextLine();
	System.out.print("혜택을 입력하시오");
	String gift = sc.nextLine();
	System.out.print("통신방식을 입력하시오");
	String commutype = sc.nextLine();
	System.out.print("데이터의 양을 입력하시오");
	String data = sc.nextLine();
	
	LGDTO lg = new LGDTO();
	lg.setFeeNm(name);
	lg.setPrice(price);
	lg.setGift(gift);
	lg.setCommuniType(commutype);
	lg.setFeeData(data);
	return lg;
	
}
private static phoneDTO inputPhones() {
	
	Scanner sc = new Scanner(System.in);
	System.out.print("OS를 입력세요! ");
	String os = sc.nextLine();
	System.out.println("출고가를 입력하세요");
	String factoryPrice = sc.nextLine();
	System.out.println("공시지원금을 입력하세요");
	String phonePriceWithFee = sc.nextLine();
	System.out.println("통신방식을 입력하세요");
	String phoneCommunicationType = sc.nextLine();
	System.out.println("내부용량을 입력하세요");
	String internalStroage = sc.nextLine();
	System.out.println("회사코드를 입력해주세요");
	int companyCode = sc.nextInt();
	System.out.println("핸드폰 이름을 입력해주세요");
	String phoneName = sc.nextLine();
	
	phoneDTO phones = new phoneDTO();
	phones.setOperationSystem(os);
	phones.setFactoryPrice(factoryPrice);
	phones.setPhonePriceWithFee(phonePriceWithFee);
	phones.setCommunicationType(phoneCommunicationType);
	phones.setInternalStorage(internalStroage);
	phones.setCompanyCode(companyCode);
	phones.setPhoneName(phoneName);
	
	return phones;
}


private static phoneCompanyDTO inputphonecompany() {
	
	Scanner sc = new Scanner(System.in);
	System.out.println("회사 이름을 입력하세요");
	String name = sc.nextLine();
	System.out.println("회사 주소를 입력하세요");
	String companyAdd = sc.nextLine();
	
	
	phoneCompanyDTO phonecompany = new phoneCompanyDTO();
	phonecompany.setCompanyName(name);
	phonecompany.setCompanyAdd(companyAdd);
	
	return phonecompany;

	
}

//----------------------------- update
private static TelecomDTO modifycompany() {
	
	Scanner sc = new Scanner(System.in);
	System.out.println("수정할 코드를 입력하세요");
	int code = sc.nextInt();
	System.out.print("수정할 통신회사의 이름을 입력하시오. ");
	String name = sc.nextLine();
	System.out.print("수정할 통신회사의 주소를 입력하시오. ");
	String add = sc.nextLine();
	
	TelecomDTO telecomDTO = new TelecomDTO();
	telecomDTO.setCompanyNo(code);
	telecomDTO.setCompanyNm(name);
	telecomDTO.setCompanyAdd(add);
	
	return telecomDTO;

}

private static SKTDTO modifyfee() {
	
	Scanner sc = new Scanner(System.in);
	System.out.println("수정할 요금제 번호를 입력하세요");
	int code = sc.nextInt();
	
	System.out.print("요금제 이름을 입력하시오 ");
	String name = sc.nextLine();
	System.out.print("월 요금을 입력하시오 ");
	String price = sc.nextLine();
	System.out.print("혜택을 입력하시오");
	String gift = sc.nextLine();
	System.out.print("통신방식을 입력하시오");
	String commutype = sc.nextLine();
	System.out.println("데이터의 양을 입력하시오");
	String data = sc.nextLine();
	
	SKTDTO skt = new SKTDTO();
	skt.setFeeNo(code);
	skt.setFeeNm(name);
	skt.setPrice(price);
	skt.setGift(gift);
	skt.setCommuniType(commutype);
	skt.setFeedata(data);
	return skt;
	}

private static KTDTO modifyfee2() {
	
	Scanner sc = new Scanner(System.in);
	System.out.println("수정할 요금제 번호를 입력하세요");
	int code = sc.nextInt();
	
	System.out.print("요금제 이름을 입력하시오 ");
	String name = sc.nextLine();
	System.out.print("월 요금을 입력하시오 ");
	String price = sc.nextLine();
	System.out.print("혜택을 입력하시오");
	String gift = sc.nextLine();
	System.out.print("통신방식을 입력하시오");
	String commutype = sc.nextLine();
	System.out.println("데이터의 양을 입력하시오");
	String data = sc.nextLine();
	
	KTDTO kt = new KTDTO();
	kt.setFeeNo(code);
	kt.setFeeNm(name);
	kt.setPrice(price);
	kt.setGift(gift);
	kt.setCommuniType(commutype);
	kt.setFeedata(data);
	return kt;
	}
private static LGDTO modifyfee3() {
	

	Scanner sc = new Scanner(System.in);
	System.out.println("수정할 요금제 번호를 입력하세요");
	int code = sc.nextInt();
	System.out.print("요금제 이름을 입력하시오 ");
	String name = sc.nextLine();
	System.out.print("월 요금을 입력하시오 ");
	String price = sc.nextLine();
	System.out.print("혜택을 입력하시오");
	String gift = sc.nextLine();
	System.out.print("통신방식을 입력하시오");
	String commutype = sc.nextLine();
	System.out.print("데이터의 양을 입력하시오");
	String data = sc.nextLine();
	
	LGDTO lg = new LGDTO();
	lg.setFeeNo(code);
	lg.setFeeNm(name);
	lg.setPrice(price);
	lg.setGift(gift);
	lg.setCommuniType(commutype);
	lg.setFeeData(data);
	return lg;
	
}

private static phoneDTO modifyPhones() {
	
	Scanner sc = new Scanner(System.in);
	System.out.println("수정할 핸드폰의 코드를 입력해주세요");
	int code = sc.nextInt();
	System.out.print("OS를 입력세요! ");
	String os = sc.nextLine();
	System.out.println("출고가를 입력하세요");
	String factoryPrice = sc.nextLine();
	System.out.println("공시지원금을 입력하세요");
	String phonePriceWithFee = sc.nextLine();
	System.out.println("통신방식을 입력하세요");
	String phoneCommunicationType = sc.nextLine();
	System.out.println("내부용량을 입력하세요");
	String internalStroage = sc.nextLine();
	System.out.println("회사코드를 입력해주세요");
	int companyCode = sc.nextInt();
	System.out.println("핸드폰 이름을 입력해주세요");
	String phoneName = sc.nextLine();
	
	phoneDTO phones = new phoneDTO();
	phones.setPhoneNo(code);
	phones.setOperationSystem(os);
	phones.setFactoryPrice(factoryPrice);
	phones.setPhonePriceWithFee(phonePriceWithFee);
	phones.setCommunicationType(phoneCommunicationType);
	phones.setInternalStorage(internalStroage);
	phones.setCompanyCode(companyCode);
	phones.setPhoneName(phoneName);
	
	return phones;
}
private static phoneCompanyDTO modifyphonecompany() {
	
	Scanner sc = new Scanner(System.in);
	System.out.println("수정할 제조사의 코드를 입력하세요");
	int code = sc.nextInt();
	System.out.println("회사 이름을 입력하세요");
	String name = sc.nextLine();
	System.out.println("회사 주소를 입력하세요");
	String companyAdd = sc.nextLine();
	
	
	phoneCompanyDTO phonecompany = new phoneCompanyDTO();
	phonecompany.setCompanyNo(code);
	phonecompany.setCompanyName(name);
	phonecompany.setCompanyAdd(companyAdd);
	
	return phonecompany;

	
}

//------------------------------------------- delete

private static int deleteTeleCom() {
	
	Scanner sc = new Scanner(System.in);
	System.out.println("삭제할 코드를 입력하세요");
	int code = sc.nextInt();

	return code;
}

private static int deleteSkt() {
	
	Scanner sc = new Scanner(System.in);
	System.out.println("삭제할 코드를 입력하세요");
	int code = sc.nextInt();

	return code;
	
	
}

private static int deleteKt() {
	
	Scanner sc = new Scanner(System.in);
	System.out.println("삭제할 코드를 입력하세요");
	int code = sc.nextInt();

	return code;
	
	
}


private static int deleteLg() {
	
	Scanner sc = new Scanner(System.in);
	System.out.println("삭제할 코드를 입력하세요");
	int code = sc.nextInt();

	return code;
	
	
}


private static int deletePhones() {
	
	Scanner sc = new Scanner(System.in);
	System.out.println("삭제할 코드를 입력하세요");
	int code = sc.nextInt();

	return code;
	
	
}

private static int deletePhoneCompany() {
	
	Scanner sc = new Scanner(System.in);
	System.out.println("삭제할 코드를 입력하세요");
	int code = sc.nextInt();

	return code;
	
	
}







}
