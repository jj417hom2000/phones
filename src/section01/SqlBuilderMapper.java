package section01;

import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;



import common.KTDTO;
import common.LGDTO;
import common.SKTDTO;
import common.TelecomDTO;
import common.phoneCompanyDTO;
import common.phoneDTO;

public interface SqlBuilderMapper {
	
	@InsertProvider(type=SqlBuilderProvider.class, method="registcom")
	int companyAdd(TelecomDTO telecom);
	
	@InsertProvider(type=SqlBuilderProvider.class, method="registSKT")
	int registSKT(SKTDTO skt);
	
	@InsertProvider(type=SqlBuilderProvider.class, method="registKT")
	int registKT(KTDTO kt);
	
	
	@InsertProvider(type=SqlBuilderProvider.class, method="registLG")
	int registLG(LGDTO LG);
	
	
	@InsertProvider(type=SqlBuilderProvider.class, method="registPhone")
	int registPhone(phoneDTO phones);
	
	
	@InsertProvider(type=SqlBuilderProvider.class, method="registPhoneCompany")
	int registPhoneCompany(phoneCompanyDTO phonecompanyDTO);
	
	

	@UpdateProvider(type=SqlBuilderProvider.class, method="modifycom")
	int modifycom(TelecomDTO telecom);
	
	@UpdateProvider(type=SqlBuilderProvider.class, method="modifySKT")
	int modifySKT(SKTDTO skt);
	@UpdateProvider(type=SqlBuilderProvider.class, method="modifyKT")
	int modifyKT(KTDTO kt);
	@UpdateProvider(type=SqlBuilderProvider.class, method="modifyLG")
	int modifyLG(LGDTO telecom);
	@UpdateProvider(type=SqlBuilderProvider.class, method="modifyPhone")
	int modifyPhoneCompany(TelecomDTO telecom);
	@UpdateProvider(type=SqlBuilderProvider.class, method="modifyPhoneCompany")
	int modifyPhone(TelecomDTO telecom);

	@DeleteProvider(type=SqlBuilderProvider.class, method="deletecom")
	int deletecom(@Param("companyNO") int companyNo);
	
	@DeleteProvider(type=SqlBuilderProvider.class, method="deletecom")
	int deleteSKT(@Param("feeNO") int companyNo);
	@DeleteProvider(type=SqlBuilderProvider.class, method="deletecom")
	int deleteKT(@Param("feeNO") int companyNo);
	@DeleteProvider(type=SqlBuilderProvider.class, method="deletecom")
	int deleteLG(@Param("feeNO") int companyNo);
	@DeleteProvider(type=SqlBuilderProvider.class, method="deletecom")
	int deletePhone(@Param("phoneNO") int companyNo);
	@DeleteProvider(type=SqlBuilderProvider.class, method="deletecom")
	int deletePhoneCompany(@Param("companyNO") int companyNo);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
