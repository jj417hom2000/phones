package section01;

import org.apache.ibatis.jdbc.SQL;

public class SelectBuilderProvider {
	
	public String selectTelecomMenu() {
		
		
		return new SQL()
				.SELECT("COMPANY_NO")
				.SELECT("COMPANY_NAME")
				.SELECT("COMMUNICATION_TYPE")
				.FROM("COMMUNICATION_COMPANY").toString();
		
		
		
	}
	
public String selectPhoneMenu() {
		
		
		return new SQL()
				.SELECT("PHONE_NO")
				.SELECT("OPERATION_SYSTEM")
				.SELECT("FACTORY_PRICE")
				.SELECT("PHONE_PRICE_WITH_FEE")
				.SELECT("PHONE_COMMUN_TYPE")
				.SELECT("COMPANY_CODE")
				.SELECT("INTERNAL_STORAGE")
				.SELECT("PHONE_NAME")
				.FROM("PHONES").toString();
		
		
		
	}
	
public String selectSkt()	{
	
	return new SQL()
			.SELECT("FEE_NO")
			.SELECT("FEE_NM")
			.SELECT("FEE_PRICE")
			.SELECT("FEE_GIFT")
			.SELECT("FEE_COMMUNICATION_TYPE")
			.SELECT("FEE_DATA")
			.FROM("SK_TELECOM").toString();
	

}
	
public String selectKt()	{
	
	return new SQL()
			.SELECT("FEE_NO")
			.SELECT("FEE_NM")
			.SELECT("FEE_PRICE")
			.SELECT("FEE_GIFT")
			.SELECT("FEE_COMMUNICATION_TYPE")
			.SELECT("FEE_DATA")
			.FROM("KOREA_TELECOM").toString();
	

}

public String selectLg()	{
	
	return new SQL()
			.SELECT("FEE_NO")
			.SELECT("FEE_NM")
			.SELECT("FEE_PRICE")
			.SELECT("FEE_GIFT")
			.SELECT("FEE_COMMUNICATION_TYPE")
			.SELECT("FEE_DATA")
			.FROM("LG_TELECOM").toString();
	

}
	
public String selectPhoneCompany() {
	
	
	return new SQL()
			.SELECT("COMPANY_NO")
			.SELECT("COMPANY_NAME")
			.SELECT("COMPANY_ADD")
			.FROM("PHONE_COMPANY").toString();
	
	
	
}
	

}
