package section01;

import static common.Template.getSqlSession;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import common.KTDTO;
import common.LGDTO;
import common.SKTDTO;
import common.TelecomDTO;
import common.phoneCompanyDTO;
import common.phoneDTO;


public class SelectBuilderService {
private SelectBuilderMapper mapper;
	
	public void phoneSelect() {
		
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(SelectBuilderMapper.class);
		
		List<phoneDTO> phoneList = mapper.selectAllMenu();
		
		if(phoneList != null && !phoneList.isEmpty()) {
			for(phoneDTO phones : phoneList) {
				System.out.println(phones);
			}
		} else {
			System.out.println("검색 결과가 존재하지 않습니다.");
		}
		
		sqlSession.close();
	}
	
	
	public void TelecomSelect() {
		
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(SelectBuilderMapper.class);
		
		List<TelecomDTO> telecomList = mapper.selectAllMenu();
		
		if(telecomList != null && !telecomList.isEmpty()) {
			for(TelecomDTO telecom : telecomList) {
				System.out.println(telecom);
			}
		} else {
			System.out.println("검색 결과가 존재하지 않습니다.");
		}
		
		sqlSession.close();
	}

	
	public void selectSkt() {
		
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(SelectBuilderMapper.class);
		
		List<SKTDTO> sktList = mapper.selectAllMenu();
		
		if(sktList != null && !sktList.isEmpty()) {
			for(SKTDTO skt : sktList) {
				System.out.println(skt);
			}
		} else {
			System.out.println("검색 결과가 존재하지 않습니다.");
		}
		
		sqlSession.close();
		
}
	
public void selectKt() {
		
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(SelectBuilderMapper.class);
		
		List<KTDTO> ktList = mapper.selectAllMenu();
		
		if(ktList != null && !ktList.isEmpty()) {
			for(KTDTO kt : ktList) {
				System.out.println(kt);
			}
		} else {
			System.out.println("검색 결과가 존재하지 않습니다.");
		}
		
		sqlSession.close();
		

	
	
	
	
	
}

public void selectLg() {
	
	SqlSession sqlSession = getSqlSession();
	mapper = sqlSession.getMapper(SelectBuilderMapper.class);
	
	List<LGDTO> lgList = mapper.selectAllMenu();
	
	if(lgList != null && !lgList.isEmpty()) {
		for(LGDTO lg : lgList) {
			System.out.println(lg);
		}
	} else {
		System.out.println("검색 결과가 존재하지 않습니다.");
	}
	
	sqlSession.close();

}

public void selectPhoneCompany() {
	
	SqlSession sqlSession = getSqlSession();
	mapper = sqlSession.getMapper(SelectBuilderMapper.class);
	
	List<phoneCompanyDTO> comList = mapper.selectAllMenu();
	
	if(comList != null && !comList.isEmpty()) {
		for(phoneCompanyDTO com : comList) {
			System.out.println(com);
		}
	} else {
		System.out.println("검색 결과가 존재하지 않습니다.");
	}
	
	sqlSession.close();
}


}