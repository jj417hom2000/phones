package section01;

import org.apache.ibatis.jdbc.SQL;

import common.TelecomDTO;

public class SqlBuilderProvider {
	
	public String registcom() {
		
		SQL sql = new SQL();
		
		sql.INSERT_INTO("COMMUNICATION_COMPANY")
		.VALUES( "COMPANY_NO" , "SEQ_COMPANY_NO.NEXTVAL")
		.VALUES("COMPANY_NAME", "#{companyName}")
		.VALUES("COMMUNICATION_TYPE", "#{communicationType}");
		
		
		return sql.toString();
		
	}
	
	public String modifycom(TelecomDTO telecom) {
		SQL sql = new SQL();
		
		sql.UPDATE("COMMUNICATION_COMPANY");
		
		if(telecom.getCompanyNm() != null && "".equals(telecom.getCompanyNm()))
		{ sql.SET("Company_NAME = #{companyName}");
		}
		if(telecom.getCompanyAdd() != null && "".equals(telecom.getCompanyAdd())) {
			sql.SET("Company_ADD = #{companyAdd}");
		}
		
		sql.WHERE("COMPANY_NO = #{companyNo}");
		return sql.toString();
		
	}
	
	public String deletecom() {
		
		SQL sql = new SQL();
		
		sql.DELETE_FROM("COMMUNICATION_COMPANY")
		.WHERE("COMPANY_NO = #{companyNo}");
		
		return sql.toString();
	}
	
	
	public String registSKT() {
		
		SQL sql = new SQL();
		
		sql.INSERT_INTO("SK_TELECOM")
		.VALUES("FEE_NO", "SEQ_FEE_NO.NEXTVAL")
		.VALUES("FEE_NM", "#{feeName}")
		.VALUES("DATA_TYPE", "#{dataType}")
		.VALUES("FEE_PRICE", "#{feeprice}")
		.VALUES("FEE_GIFT", "#{feegift}")
		.VALUES("FEE_COMMUNICATION_TYPE", "#{feecommunicationType}")
		.VALUES("FEE_DATA", "#{feedata}");
		return sql.toString();
		
	}
	
	
public String registKT() {
		
		SQL sql = new SQL();
		
		sql.INSERT_INTO("KOREA_TELECOM")
		.VALUES("FEE_NO", "SEQ_FEE_NO.NEXTVAL")
		.VALUES("FEE_NM", "#{feeName}")
		.VALUES("DATA_TYPE", "#{dataType}")
		.VALUES("FEE_PRICE", "#{feeprice}")
		.VALUES("FEE_GIFT", "#{feegift}")
		.VALUES("FEE_COMMUNICATION_TYPE", "#{feecommunicationType}")
		.VALUES("FEE_DATA", "#{feedata}");
		
		return sql.toString();
		
	}
	
	
public String registLG() {
	
	SQL sql = new SQL();
	
	sql.INSERT_INTO("LG_TELECOM")
	.VALUES("FEE_NO", "SEQ_FEE_NO.NEXTVAL")
	.VALUES("FEE_NM", "#{feeName}")
	.VALUES("DATA_TYPE", "#{dataType}")
	.VALUES("FEE_PRICE", "#{feeprice}")
	.VALUES("FEE_GIFT", "#{feegift}")
	.VALUES("FEE_COMMUNICATION_TYPE", "#{feecommunicationType}")
	.VALUES("FEE_DATA", "#{feedata}");
	
	return sql.toString();
	
}
	

public String registPhone() {
	
	SQL sql = new SQL();
	
	sql.INSERT_INTO("PHONES")
	.VALUES("PHONES_NO", "PHONES_NO.NEXTVAL")
	.VALUES("OPERATION_SYSTEM", "#{operationSystem}")
	.VALUES("FACTORY_PRICE", "#{factoryPrice}")
	.VALUES("PHONE_PRICE_WITH_FEE", "#{phonePriceWithFee}")
	.VALUES("PHONE_COMMUN_TYPE", "#{phoneCommunType}")
	.VALUES("COMPANY_CODE", "#{companyCode}")
	.VALUES("INTERNAL_STORAGE", "#{internalStorage}" )
	.VALUES("PHONE_NAME", "#{phoneName}");
	
	
	return sql.toString();
	
}

public String registPhoneCompany() {
	
	SQL sql = new SQL();
	
	sql.INSERT_INTO("PHONE_COMPANY")
	.VALUES("PHONES_NO", "PHONE_COMPANE_NO.NEXTVAL")
	.VALUES("COMPANY_NAME", "#{companyName}")
	.VALUES("COMPANY_ADD", "#{conpanyAdd}");
	
	
	return sql.toString();
	
	
	
	
}
	
public String modifySKT() {
	
	SQL sql = new SQL();
	
	sql.UPDATE("SK_TELECOM")
	.SET("FEE_NM", "#{feeName}")
	.SET("DATA_TYPE", "#{dataType}")
	.SET("FEE_PRICE", "#{feeprice}")
	.SET("FEE_GIFT", "#{feegift}")
	.SET("FEE_COMMUNICATION_TYPE", "#{feecommunicationType}")
	.SET("FEE_DATA", "#{feedata}")
	.WHERE("FEE_NO = #{feeNo}");
	return sql.toString();
	
}	
	
public String modifyKT() {
	
	SQL sql = new SQL();
	
	
	sql.UPDATE("KOREA_TELECOM")
	.SET("FEE_NM", "#{feeName}")
	.SET("DATA_TYPE", "#{dataType}")
	.SET("FEE_PRICE", "#{feeprice}")
	.SET("FEE_GIFT", "#{feegift}")
	.SET("FEE_COMMUNICATION_TYPE", "#{feecommunicationType}")
	.SET("FEE_DATA", "#{feedata}")
	.WHERE("FEE_NO = #{feeNo}");
	return sql.toString();
	
	
	
	
}

public String modifyLG() {
	
	SQL sql = new SQL();
	
	
	sql.UPDATE("LG_TELECOM")
	.SET("FEE_NM", "#{feeName}")
	.SET("DATA_TYPE", "#{dataType}")
	.SET("FEE_PRICE", "#{feeprice}")
	.SET("FEE_GIFT", "#{feegift}")
	.SET("FEE_COMMUNICATION_TYPE", "#{feecommunicationType}")
	.SET("FEE_DATA", "#{feedata}")
	.WHERE("FEE_NO = #{feeNo}");
	return sql.toString();
	
	
	
	
	
	
}

public String modifyPhone() {
	
	SQL sql = new SQL();
	
	sql.UPDATE("PHONES")
	.SET("OPERATION_SYSTEM", "#{operationSystem}")
	.SET("FACTORY_PRICE", "#{factoryPrice}")
	.SET("PHONE_PRICE_WITH_FEE", "#{phonePriceWithFee}")
	.SET("PHONE_COMMUN_TYPE", "#{phoneCommunType}")
	.SET("COMPANY_CODE", "#{companyCode}")
	.SET("INTERNAL_STORAGE", "#{internalStorage}" )
	.SET("PHONE_NAME", "#{phoneName}")
	.WHERE("PHONE_NO = #{phoneNo}");
	
	return sql.toString();
	
}

public String modifyPhoneCompany() {
	
	SQL sql = new SQL();
	
	sql.UPDATE("PHONE_COMPANY")
	.SET("COMPANY_NAME", "#{companyName}")
	.SET("COMPANY_ADD", "#{companyAdd}")
	.WHERE("COMPANY_NO = #{companyNo}");
	
	
	return sql.toString();}


public String deleteKT() {
	SQL sql = new SQL();
	
	
	sql.DELETE_FROM("KOREA_TELECOM")
	.WHERE("FEE_NO = #{feeNo} ");
	
	return sql.toString();
	
}



public String deleteSKT() {
	SQL sql = new SQL();
	
	
	sql.DELETE_FROM("SK_TELECOM")
	.WHERE("FEE_NO = #{feeNo} ");
	
	return sql.toString();
	
}



public String deleteLG() {
	SQL sql = new SQL();
	
	
	sql.DELETE_FROM("LG_TELECOM")
	.WHERE("FEE_NO = #{feeNo} ");
	
	return sql.toString();
	
}

public String deletePhone() {
	SQL sql = new SQL();
	
	
	sql.DELETE_FROM("PHONES")
	.WHERE("PHONE_NAME LIKE '|| #{phoneName} || '%'");
	
	return sql.toString();
	
}

public String deletePhoneCompany() {
	
	SQL sql = new SQL();
	
	sql.DELETE_FROM("PHONE_COMPANY")
	.WHERE("COMPANY_NO = #{companyNo}");
	
	return sql.toString();




}




}





